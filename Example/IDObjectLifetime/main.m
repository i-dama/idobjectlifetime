//
//  main.m
//  IDObjectLifetime
//
//  Created by Ivan Damjanović on 05/29/2015.
//  Copyright (c) 2014 Ivan Damjanović. All rights reserved.
//

@import UIKit;
#import "IDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
