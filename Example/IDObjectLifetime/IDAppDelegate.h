//
//  IDAppDelegate.h
//  IDObjectLifetime
//
//  Created by CocoaPods on 05/29/2015.
//  Copyright (c) 2014 Ivan Damjanović. All rights reserved.
//

@import UIKit;

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
