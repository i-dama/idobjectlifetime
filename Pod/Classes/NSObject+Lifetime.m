//
//  NSObject+Lifetime.m
//  Pods
//
//  Created by Dama on 29/05/15.
//
//

#import "NSObject+Lifetime.h"
#import <objc/runtime.h>

static char appendObjectKey;

@implementation NSObject (Lifetime)

+ (void)makeObject:(id)anObject haveTheSameLifetimeAsObject:(id)hostObject
{
    objc_setAssociatedObject(hostObject, &appendObjectKey, anObject, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (void)cancelObjectLietimeOnObject:(id)hostObject
{
    objc_setAssociatedObject(hostObject, &appendObjectKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
