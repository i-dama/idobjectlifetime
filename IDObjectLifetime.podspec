Pod::Spec.new do |s|
  s.name             = "IDObjectLifetime"
  s.version          = "0.1.0"
  s.summary          = "Managing object lifetime"
  s.description      = <<-DESC
                       Enables sticking objects to another object to prevent them from deallocating too soon
                       DESC
  s.homepage         = "https://bitbucket.com/i-dama/idobjectlifetime"
  s.license          = 'MIT'
  s.author           = { "Ivan Damjanović" => "ivan.damjanovic@infinum.hr" }
  s.source           = { :git => "https://bitbucket.org/i-dama/idobjectlifetime", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/damaofficial'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'IDObjectLifetime' => ['Pod/Assets/*.png']
  }
end
